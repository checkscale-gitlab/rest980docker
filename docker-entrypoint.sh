#!/bin/sh
set -e

echo "----------------------------------------------------------------------------------"
echo "--- REST980 for Docker: The REST interface to your iRobot Roomba series robot. ---"
echo "----------------------------------------------------------------------------------"

# Ensure the variable ROBOT_IP is set.
if [ -z "$ROBOT_IP" ]; then
    echo "Set ROBOT_IP to your iRobot Roomba robot's IP !"
    exit 1
fi

# REST980 requires variables BLID and PASSWORD to be set, too.
if [ -z "$BLID" ] || [ -z "$PASSWORD" ]; then
    # Let the user lookup the BLID and PASSWORD.
    echo "WARNING: BLID and PASSWORD are not set. Set the environment variables your robot's credentials."
    echo ""
    echo "You may lookup the password by executing this command within the next 90 seconds."
    echo "    docker exec -it <Your REST980 containter's ID or name> npm run getpassword $ROBOT_IP"
    echo ""
    echo "Then follow the shown steps. Good luck."
    echo ""

    COUNTER=9
    while [ $COUNTER -gt 0 ]
    do
        echo "    ${COUNTER}0 sec. left to perform the action."
        COUNTER=$((COUNTER-1))
        sleep 10s
    done

    echo ""
    echo "Exiting."
    exit 2
else
    # Check for basic auth environment variables
    if [ -n "$BASIC_AUTH_USER" ] && [ -z "$BASIC_AUTH_PASS" ]; then
        echo "You'll have to provide a password for the basic authentication, too."
        exit 3
    fi
    
    if [ -z "$BASIC_AUTH_USER" ] && [ -n "$BASIC_AUTH_PASS" ]; then
        echo "You'll have to provide a username for the basic authentication, too."
        exit 4
    fi

    # Start the REST980 interface
    echo "Starting REST980 for your robot at IP $ROBOT_IP ..."
    exec "$@"
fi
